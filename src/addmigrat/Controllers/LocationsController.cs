using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using addmigrat.Models;
using System;
using System.Collections.Generic;


namespace addmigrat.Controllers
{
    public class LocationsController : Controller
    {
        private ApplicationDbContext _context;

        public LocationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Locations
        public IActionResult Index(string sortOrder,string SearchString,string place) 
        {
            ViewBag.countrySort = String.IsNullOrEmpty(sortOrder) ? "country_desc" : "";
            ViewBag.placeSort = sortOrder == "Place" ? "place_desc" : "Place";
            
            //drop down list populating
            var placelist = new List<String>();

            var places = from m in _context.Locations
                         orderby m.Place
                         select m.Place;

            placelist.AddRange(places);

            ViewBag.place = new SelectList(placelist);

            //search operations
            var locations = from m in _context.Locations
                         select m;

            if (!String.IsNullOrEmpty(SearchString) || !String.IsNullOrEmpty(place))
            {
                locations = locations.Where(s => (s.Country.Contains(SearchString)) && (s.Place == place));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    locations = locations.OrderByDescending(s => s.Country);
                    break;
                case "Place":
                    locations = locations.OrderBy(s => s.Place);
                    break;
                case "place_desc":
                    locations = locations.OrderByDescending(s => s.Place);
                    break;
                default:
                    locations = locations.OrderBy(s => s.Country);
                    break;
            }
           
            //int pageSize = 3;
            //int pageNumber = (page ?? 1);
            //return View(locations.ToPagedList(pageNumber, pageSize));

            return View(locations);
            //return View(_context.Locations.ToList());
        }


        // GET: Locations/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Location location = _context.Locations.Single(m => m.LocationID == id);
            if (location == null)
            {
                return HttpNotFound();
            }

            return View(location);
        }

        // GET: Locations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Locations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Location location)
        {
            if (ModelState.IsValid)
            {
                _context.Locations.Add(location);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        // GET: Locations/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Location location = _context.Locations.Single(m => m.LocationID == id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Location location)
        {
            if (ModelState.IsValid)
            {
                _context.Update(location);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        // GET: Locations/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Location location = _context.Locations.Single(m => m.LocationID == id);
            if (location == null)
            {
                return HttpNotFound();
            }

            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Location location = _context.Locations.Single(m => m.LocationID == id);
            _context.Locations.Remove(location);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
